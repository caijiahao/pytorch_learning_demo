#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@File    :   device.py    
@Contact :   raogx.vip@hotmail.com
@License :   (C)Copyright 2017-2018, Liugroup-NLPR-CASIA

@Modify Time      @Author    @Version    @Desciption
------------      -------    --------    -----------
2021/12/14 12:13 上午   caijiahao      1.0         Bytedancer
'''


# import lib


def device():
    import torch

    """自动选择训练模式，尽可能使用GPU进行运算"""
    if torch.cuda.is_available():
        return torch.device('cuda:0')
    else:
        return torch.device('cpu')
