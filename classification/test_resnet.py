#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@File    :   test_resnet.py    
@Contact :   raogx.vip@hotmail.com
@License :   (C)Copyright 2017-2018, Liugroup-NLPR-CASIA

@Modify Time      @Author    @Version    @Desciption
------------      -------    --------    -----------
2021/12/25 11:30 下午   caijiahao      1.0         Bytedancer
'''

# import lib
from tkinter import *
from tkinter import filedialog
from PIL import Image, ImageTk

if __name__ == "__main__":
    root = Tk()
    root.title('请选择图片')
    frame = Frame(root, bd=2, relief=SUNKEN)
    frame.grid_rowconfigure(0, weight=1)
    frame.grid_columnconfigure(0, weight=1)
    canvas = Canvas(frame, bd=0)
    canvas.grid(row=0, column=0, sticky=N + S + E + W)
    frame.pack(fill=BOTH, expand=1)


    def printcoords():
        import shutil, os
        from test import test_resnet
        File = filedialog.askopenfilename(parent=root, initialdir=os.getcwd(), title='选择图片.')
        img = Image.open(File)
        out = img.resize((336, 192), Image.ANTIALIAS)  # resize image with high-quality
        filename = ImageTk.PhotoImage(out)
        canvas.image = filename
        canvas.create_image(0, 0, anchor='nw', image=filename)

        dir = os.getcwd() + r'\test'
        shutil.rmtree(dir, True)
        os.makedirs(os.path.join(os.getcwd(), 'test'))

        shutil.copyfile(File, dir + r'\image')
        result = test_resnet.test()
        print(result)
        root.title(result)


    Button(root, text='选择', command=printcoords).pack()
    root.mainloop()
